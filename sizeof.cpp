#include <iostream>

int main() {
    std::cout << "Boolean: " << sizeof(bool) << "\n";
    std::cout << "Character: " << sizeof(char) << '\n';
    std::cout << "Integer: " << sizeof(int) << "\n";
    std::cout << "Double: " << sizeof(double) << "\n";
    std::cout << "Unsigned: " << sizeof(unsigned) << "\n";
    std::cout << "String: " << sizeof(std::string) << "\n";
}
