#include <iostream>

double square(double x){
    return x*x;
}

void print_square(double x){
    std::cout << "the square of " << x << " is " << square(x) << "\n";
}

int main() {
    double n = 0;
    std::cin >> n;
    print_square(n);
}
