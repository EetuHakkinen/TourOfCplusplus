#include <iostream>
#include <vector>

int sum(std::vector v){
    int s = 0;
    for(int i = 0; i < v.length; ++i){
        s += v[i];
    }

    return s;
}

int main(){
    std::vector<int> v {1, 2, 3};
    std::cout << "Value: " << sum(v);
}


